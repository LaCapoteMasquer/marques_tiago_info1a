"""
    Fichier : gestion_sim_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les sim.
"""
import re
import sys

from flask import flash
from flask import render_template
from flask import request
from flask import url_for
from flask import redirect

from APP_FILMS import obj_mon_application
from APP_FILMS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_FILMS.erreurs.msg_erreurs import *
from APP_FILMS.erreurs.exceptions import *
from APP_FILMS.essais_wtf_forms.wtf_forms_1 import MonPremierWTForm
from APP_FILMS.sim.gestion_sim_wtf_forms import FormWTFAjouterSim

"""
    Auteur : OM 2021.03.16
    Définition d'une "route" /sim_afficher
    
    Test : ex : http://127.0.0.1:5005/sim_afficher
    
    Paramètres : order_by : ASC : Ascendant, DESC : Descendant
                ID_sim_sel = 0 >> tous les sim.
                ID_sim_sel = "n" affiche le genre dont l'id est "n"
"""


@obj_mon_application.route("/sim_afficher/<string:order_by>/<int:ID_sim_sel>", methods=['GET', 'POST'])
def sim_afficher(order_by, ID_sim_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion sim ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionSim {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if order_by == "ASC" and ID_sim_sel == 0:
                    strsql_sim_afficher = """SELECT ID_Sim, Modele FROM t_sim ORDER BY ID_Sim ASC"""
                    mc_afficher.execute(strsql_sim_afficher)
                elif order_by == "ASC":
                    # C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
                    # la commande MySql classique est "SELECT * FROM t_sim"
                    # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
                    # donc, je précise les champs à afficher
                    # Constitution d'un dictionnaire pour associer l'id du genre sélectionné avec un nom de variable
                    valeur_ID_sim_selected_dictionnaire = {"value_ID_sim_selected": ID_sim_sel}
                    strsql_sim_afficher = """SELECT ID_Sim, Modele FROM t_sim  WHERE ID_Sim = %(value_ID_sim_selected)s"""

                    mc_afficher.execute(strsql_sim_afficher, valeur_ID_sim_selected_dictionnaire)
                else:
                    strsql_sim_afficher = """SELECT ID_Sim, Modele FROM t_sim ORDER BY ID_Sim DESC"""

                    mc_afficher.execute(strsql_sim_afficher)

                data_sim = mc_afficher.fetchall()

                print("data_sim ", data_sim, " Type : ", type(data_sim))

                # Différencier les messages si la table est vide.
                if not data_sim and ID_sim_sel == 0:
                    flash("""La table "t_sim" est vide. !!""", "warning")
                elif not data_sim and ID_sim_sel > 0:
                    # Si l'utilisateur change l'ID_Sim dans l'URL et que le genre n'existe pas,
                    flash(f"La sim demandé n'existe pas !!", "warning")
                else:
                    # Dans tous les autres cas, c'est que la table "t_sim" est vide.
                    # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                    flash(f"Données sim affichés !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            flash(f"RGG Exception {erreur}")
            raise Exception(f"RGG Erreur générale. {erreur}")
            raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page "HTML" au serveur.
    return render_template("sim/sim_afficher.html", data=data_sim)


"""
    Auteur : OM 2021.03.22
    Définition d'une "route" /sim_ajouter
    
    Test : ex : http://127.0.0.1:5005/genres_ajouter
    
    Paramètres : sans
    
    But : Ajouter un sim pour un film
    
    Remarque :  Dans le champ "name_sim_html" du formulaire "sim/genres_ajouter.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/sim_ajouter", methods=['GET', 'POST'])
def sim_ajouter_wtf():
    form = FormWTFAjouterSim()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion sim ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionSim {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                name_sim_wtf = form.nom_sim_wtf.data

                name_sim = name_sim_wtf.lower()
                valeurs_insertion_dictionnaire = {"value_Modele": name_sim}
                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_sim = """INSERT INTO t_sim (ID_Sim,Modele) VALUES (NULL,%(value_Modele)s)"""
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_sim, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('sim_afficher', order_by='DESC', ID_sim_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_sim_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_sim_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_sim_crud:
            code, msg = erreur_gest_sim_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion sim CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_sim_crud.args[0]} , "
                  f"{erreur_gest_sim_crud}", "danger")

    return render_template("sim/sim_ajouter_wtf.html", form=form)
